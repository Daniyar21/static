import './App.css';
import Layout from "./components/Layout/Layout";
import {BrowserRouter, Route, Switch} from "react-router-dom";

const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path="/pages/:title"/>
                </Switch>
            </Layout>
        </BrowserRouter>
    )
};

export default App;
