import React from 'react';

const SemanticContent = ({match}) => {

        const [page, setPage] =useState([]);

        const fetchData = async ()=>{
            const response = await axios.get('https://blog-93444-default-rtdb.firebaseio.com/pages/'+match.params.title+'.json');
            setPageContent(response.data);
        };
        fetchData().catch(console.error);

        return (
            <div>
                <h1>{page.title}</h1>
                <span>{page.content}</span>
            </div>
        );
    };

export default SemanticContent;