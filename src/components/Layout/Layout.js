import React from 'react';
import './Layout.css';
import {NavLink} from "react-router-dom";

const Layout = () => {
    return (
        <>
          <header>
              <div className="logo">Logo</div>
              <div className="NavLink">
                  <NavLink to='/pages/about'>About</NavLink>
                  <NavLink to='/pages/branches'>Branches</NavLink>
                  <NavLink to='/pages/contacts'>Contacts</NavLink>
                  <NavLink to='/pages/products'>Products</NavLink>
                  <NavLink to='/pages/services'>Services</NavLink>
              </div>

          </header>
            <main className="Layout-Content">
                content
            </main>
        </>
    );
};

export default Layout;